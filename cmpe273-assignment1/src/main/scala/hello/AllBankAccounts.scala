package hello

class AllBankAccounts {

  var map = collection.mutable.Map.empty[String, UserBankAccounts];

  def SaveData(user_id : String , bankacc : BankAccount)
  {
    if (map.contains(user_id)) {
      map(user_id).SaveData(bankacc);
    } else {
      val user_bank_account = new UserBankAccounts()
      user_bank_account.SaveData(bankacc)
      map.put(user_id, user_bank_account);
    }
  }
  //def DeleteBankAccount(user_id: String, ba_id : Int)
  def DeleteBankAccount(user_id: String, ba_id : String)
  {
    map(user_id).DeleteBankAccount(ba_id);
  }
}
