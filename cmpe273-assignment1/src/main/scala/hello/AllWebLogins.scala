package hello

class AllWebLogins {

  var map = collection.mutable.Map.empty[String, UserWebLogin];

  def SaveData(user_id : String , weblogin : WebLogin)
  {
    if (map.contains(user_id)) {
      map(user_id).SaveData(weblogin);
    } else {
      val user_web_login = new UserWebLogin()
      user_web_login.SaveData(weblogin)
      map.put(user_id, user_web_login);
    }
  }
  def DeleteWebLogin(user_id: String, login_id : String)
  {
    map(user_id).DeleteWebLogin(login_id);
  }

}
