package hello

import scala.collection.mutable.ArrayBuffer


class UserIDCards {

  var cards = new  ArrayBuffer[IDCard]();
  var str = ""

  def SaveData(card : IDCard)
  {
    cards :+= card;
  }

  def getCards(): Array[IDCard] =
  {
    return cards.toArray
  }

  def DeleteCard(card_id : String)
  {

    for ( i <- 0 to (cards.length - 1) ) {
     if(cards(i).card_id == card_id) {
       cards.remove(i);
       return;
     }
    }

  }


}
