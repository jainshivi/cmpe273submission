package hello

class BankAccount{
    var ba_id = "";
    var account_name = "";
    var routing_number = "";
    var account_number = "";
    def this(ba_id : String , account_name : String , routing_number : String , account_number : String)
    {
      this();
      this.ba_id = ba_id;
      this.account_name = account_name ;
      this.routing_number = routing_number;
      this.account_number = account_number;
    }

  def getba_id() : String =
  {
    return ba_id;
  }

  def getaccount_name() : String =
  {
    return account_name;
  }

  def getrouting_number() : String =
  {
    return routing_number;
  }

  def getaccount_number() : String =
  {
    return account_number;
  }
}
