package hello

class AllIDCards {

  var map = collection.mutable.Map.empty[String, UserIDCards];

  def SaveData(user_id : String , card : IDCard)
  {
    if (map.contains(user_id)) {
      map(user_id).SaveData(card);
    } else {
      val user_id_cards = new UserIDCards()
      user_id_cards.SaveData(card)
      map.put(user_id, user_id_cards);
    }
  }
  def DeleteCard(user_id: String, card_id : String)
  {
    map(user_id).DeleteCard(card_id);
  }
}
