package hello
import scala.collection.mutable.ArrayBuffer

class UserWebLogin {

  var weblogins = new  ArrayBuffer[WebLogin]();
  var user_id = "";

  def this(user_id : String)
  {
    this();
    this.user_id = user_id;
  }

  def SaveData(weblogin : WebLogin)
  {
    weblogins :+= weblogin;
  }

  def getWebLogins(): Array[WebLogin] =
  {
    return weblogins.toArray
  }

  def DeleteWebLogin(login_id : String)
  {
    for ( i <- 0 to (weblogins.length - 1) ) {
      if(weblogins(i).login_id == login_id) {
        weblogins.remove(i);
        return;
      }
    }
  }



}
