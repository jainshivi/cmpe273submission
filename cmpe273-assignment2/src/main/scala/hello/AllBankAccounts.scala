package hello
import  com.mongodb.DBCursor;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.ApplicationContext;
import scala.collection.mutable.ArrayBuffer
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBList;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;

class AllBankAccounts {

  var map = collection.mutable.Map.empty[String, UserBankAccounts];
  var ctx: ApplicationContext = new AnnotationConfigApplicationContext(classOf[MongoConfig])
  var mongoOperations: MongoOperations = ctx.getBean("mongoTemplate").asInstanceOf[MongoOperations]


  def this(s:String) {
    this();
    val userbankacccollection = mongoOperations.getCollection("userbankaccounts");
    val cursor = userbankacccollection.find();
    while (cursor.hasNext()) {
      val obj = cursor.next();
      var u_id = obj.get("user_id").asInstanceOf[String];
      var userbankacc = new UserBankAccounts(u_id);
      var bankaccdict = obj.get("bankaccounts").asInstanceOf[BasicDBObject];
      var bankaccs = bankaccdict.get("array").asInstanceOf[BasicDBList].toArray;
      for (i <- 0 to (bankaccs.length - 1)) {
        val objbankacc = bankaccs(i).asInstanceOf[BasicDBObject];
        if (objbankacc ne null) {
          val bankacc = new BankAccount(objbankacc.get("ba_id").asInstanceOf[String], objbankacc.get("account_name").asInstanceOf[String], objbankacc.get("routing_number").asInstanceOf[String], objbankacc.get("account_number").asInstanceOf[String]);
          userbankacc.SaveData(bankacc);
        }
      }
      map.put(u_id, userbankacc);
    }
  }


  def SaveData(user_id : String , bankacc : BankAccount)
  {
    val query = new Query();
    val update = new Update();
    if (map.contains(user_id)) {
      map(user_id).SaveData(bankacc);
      query.addCriteria(Criteria.where("user_id").is(user_id));
      update.set("bankaccounts", map(user_id).bankaccounts);
      mongoOperations.updateMulti(query, update,"userbankaccounts");
    }
    else {
      val user_bank_account = new UserBankAccounts(user_id)
      user_bank_account.SaveData(bankacc)
      map.put(user_id, user_bank_account);
      mongoOperations.save(map(user_id), "userbankaccounts");
    }
  }
  //def DeleteBankAccount(user_id: String, ba_id : Int)
  def DeleteBankAccount(user_id: String, ba_id : String)
  {
    var searchUserQuery = new Query();
    val update = new Update();
    searchUserQuery.addCriteria(Criteria.where("user_id").is(user_id))
    map(user_id).DeleteBankAccount(ba_id);
    update.set("bankaccounts", map(user_id).bankaccounts);
    mongoOperations.updateMulti(searchUserQuery, update,"userbankaccounts");
  }
}
