package hello
import  com.mongodb.DBCursor;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.ApplicationContext;
import scala.collection.mutable.ArrayBuffer
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBList;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;

class AllWebLogins {

  var map = collection.mutable.Map.empty[String, UserWebLogin];
  var ctx: ApplicationContext = new AnnotationConfigApplicationContext(classOf[MongoConfig])
  var mongoOperations: MongoOperations = ctx.getBean("mongoTemplate").asInstanceOf[MongoOperations]


  def this(s:String) {
    this();
    val userwebloginscollection = mongoOperations.getCollection("userweblogins");
    println("loading allweblogins")
    val cursor = userwebloginscollection.find();
    while (cursor.hasNext()) {
      val obj = cursor.next();
      var u_id = obj.get("user_id").asInstanceOf[String];
      var userweblogin = new  UserWebLogin(u_id);
      var weblogindict = obj.get("weblogins").asInstanceOf[BasicDBObject];
      var weblogins = weblogindict.get("array").asInstanceOf[BasicDBList].toArray;
      for ( i <- 0 to (weblogins.length - 1) ) {
        val objlogin = weblogins(i).asInstanceOf[BasicDBObject];
        if(objlogin ne null) {
          val weblogin = new WebLogin(objlogin.get("login_id").asInstanceOf[String], objlogin.get("url").asInstanceOf[String], objlogin.get("login").asInstanceOf[String], objlogin.get("password").asInstanceOf[String]);
          userweblogin.SaveData(weblogin);
        }
      }
      map.put(u_id,userweblogin);
      println(map(u_id).getWebLogins().deep.mkString("\n"))
    }
  }


  def SaveData(user_id : String , weblogin : WebLogin)
  {
    val query = new Query();
    val update = new Update();
    if (map.contains(user_id)) {
      map(user_id).SaveData(weblogin);
      query.addCriteria(Criteria.where("user_id").is(user_id));
      update.set("weblogins", map(user_id).weblogins);
      mongoOperations.updateMulti(query, update,"userweblogins");
    } else {
      val user_web_login = new UserWebLogin(user_id)
      user_web_login.SaveData(weblogin)
      map.put(user_id, user_web_login);

      mongoOperations.save(map(user_id), "userweblogins");
    }
  }
  def DeleteWebLogin(user_id: String, login_id : String)
  {
    var searchUserQuery = new Query();
    val update = new Update();
    searchUserQuery.addCriteria(Criteria.where("user_id").is(user_id))
    map(user_id).DeleteWebLogin(login_id);
    update.set("weblogins", map(user_id).weblogins);
    mongoOperations.updateMulti(searchUserQuery, update,"userweblogins");
  }

  //consuming RESTful API


}
