package hello
import  com.mongodb.DBCursor;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.ApplicationContext;
import scala.collection.mutable.ArrayBuffer
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBList;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;


class AllIDCards {

  var map = collection.mutable.Map.empty[String, UserIDCards];
  var ctx: ApplicationContext = new AnnotationConfigApplicationContext(classOf[MongoConfig])
  var mongoOperations: MongoOperations = ctx.getBean("mongoTemplate").asInstanceOf[MongoOperations]

  def this(s:String) {
    this();
    val useridcardscollection = mongoOperations.getCollection("useridcards");
    val cursor = useridcardscollection.find();
    while (cursor.hasNext()) {
      val obj = cursor.next();
      var u_id = obj.get("user_id").asInstanceOf[String];
      var useridcard = new  UserIDCards(u_id);
      var idcarddict = obj.get("cards").asInstanceOf[BasicDBObject];
      var idcards = idcarddict.get("array").asInstanceOf[BasicDBList].toArray;
      for ( i <- 0 to (idcards.length - 1) ) {
        val objcard = idcards(i).asInstanceOf[BasicDBObject];
        if(objcard ne null) {
          val idcard = new IDCard(objcard.get("card_id").asInstanceOf[String], objcard.get("card_name").asInstanceOf[String], objcard.get("card_number").asInstanceOf[String], objcard.get("expiration_date").asInstanceOf[String]);
          useridcard.SaveData(idcard);
        }
      }
      map.put(u_id,useridcard);
    }
  }

  def SaveData(user_id : String , card : IDCard)
  {
    val query = new Query();
    val update = new Update();
    if (map.contains(user_id)) {

      map(user_id).SaveData(card);
      query.addCriteria(Criteria.where("user_id").is(user_id));
      update.set("cards", map(user_id).cards);
      //println(map(user_id).getCards().deep.mkString("\n"))

      mongoOperations.updateMulti(query, update,"useridcards");

    } else {
      val user_id_cards = new UserIDCards(user_id)
      user_id_cards.SaveData(card)
      map.put(user_id, user_id_cards);
      mongoOperations.save(map(user_id), "useridcards");
      update.set("cards", map(user_id).cards);

    }

  }
  def DeleteCard(user_id: String, card_id : String)
  {
    var searchUserQuery = new Query();
    val update = new Update();
    //searchUserQuery.addCriteria(Criteria.where("user_id").is(user_id).and("card_id").is(card_id))
    searchUserQuery.addCriteria(Criteria.where("user_id").is(user_id))
    map(user_id).DeleteCard(card_id);
    update.set("cards", map(user_id).cards);
    mongoOperations.updateMulti(searchUserQuery, update,"useridcards");
  }
}
