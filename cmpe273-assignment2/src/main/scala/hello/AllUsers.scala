package hello
import  com.mongodb.DBCursor;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.ApplicationContext;

class AllUsers {

  var map = collection.mutable.Map.empty[String, User];
  var ctx: ApplicationContext = new AnnotationConfigApplicationContext(classOf[MongoConfig])
  var mongoOperations: MongoOperations = ctx.getBean("mongoTemplate").asInstanceOf[MongoOperations]

  def this(s:String) {
    this();

    val usercollection = mongoOperations.getCollection("users");
    val cursor =  usercollection.find();
    while(cursor.hasNext()) {
      val obj = cursor.next();
      map.put(
        obj.get("user_id").asInstanceOf[String],
        new User(
          obj.get("user_id").asInstanceOf[String],
          obj.get("email").asInstanceOf[String],
          obj.get("password").asInstanceOf[String],
          obj.get("name").asInstanceOf[String],
          obj.get("created_at").asInstanceOf[String],
          obj.get("updated_at").asInstanceOf[String]));
      println(obj.get("user_id").asInstanceOf[String]);
    }
  }

  def SaveData(user : User)
  {

    map.put(user.user_id, user);
  }



}
