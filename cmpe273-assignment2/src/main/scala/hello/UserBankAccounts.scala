package hello
import scala.collection.mutable.ArrayBuffer

class UserBankAccounts {

  var bankaccounts = new  ArrayBuffer[BankAccount]();
  var user_id = "";

  def this(user_id:String)
  {
    this();
    this.user_id = user_id;
  }


  def SaveData(bankaccount : BankAccount)
  {
    bankaccounts :+= bankaccount;
  }

  def getBankAccounts(): Array[BankAccount] =
  {
    return bankaccounts.toArray
  }

  def DeleteBankAccount(ba_id : String)
  {

    for ( i <- 0 to (bankaccounts.length - 1) ) {
     if(bankaccounts(i).ba_id == ba_id) {
       bankaccounts.remove(i);
       return;
     }
    }

  }
}
