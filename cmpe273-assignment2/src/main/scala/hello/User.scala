package hello



class User {
    var user_id = "";//system generated
    var email = "";
    var password = "";
    var name = "";
    var created_at = "";//datetime system generated
    var updated_at = "";//datetime system generated
    //override def toString = "u-" + user_id;

  def this(user_id : String , email : String , password : String , name : String, created_at: String, updated_at: String )
  {
    this();

    this.user_id =user_id.toString;
    this.email = email;
    this.password = password;
    this.name = name;
    this.created_at = created_at;
    this.updated_at = updated_at;
  }



  def getUser_id(): String =
  {
    return user_id;
    //return toString;
  }

  def getEmail() : String =
  {
    return email;
  }

  def getPassword() : String =
  {
    return password;
  }

  def getName() : String =
  {
    return name;
  }

  def getCreated_at() : String =
  {
    return created_at ;
  }
  def getUpdated_at() : String =
  {
    return updated_at ;
  }
}
