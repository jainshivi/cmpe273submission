package hello
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.context.annotation.Configuration;
import com.mongodb.Mongo;
import org.springframework.data.authentication.UserCredentials;

@Configuration
class MongoConfig extends AbstractMongoConfiguration {

  def getDatabaseName:String = "273assignments"

  def mongo:Mongo = new Mongo("ds049150.mongolab.com:49150")

  protected override def getUserCredentials(): UserCredentials = {
    new UserCredentials("shivijain", "Welcome$123")
  }
}
