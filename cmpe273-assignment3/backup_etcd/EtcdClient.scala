package etcd

import java.net.URI
import java.io.IOException
import java.io.UnsupportedEncodingException
import java.net.URI
import java.net.URLEncoder
import java.util.ArrayList
import java.util.List
import java.util.concurrent.ExecutionException
import org.apache.http.HttpEntity
import org.apache.http.HttpResponse
import org.apache.http.StatusLine
import org.apache.http.client.config.RequestConfig
import org.apache.http.client.entity.UrlEncodedFormEntity
import org.apache.http.client.methods.HttpDelete
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPut
import org.apache.http.client.methods.HttpUriRequest
import org.apache.http.concurrent.FutureCallback
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient
import org.apache.http.impl.nio.client.HttpAsyncClients
import org.apache.http.message.BasicNameValuePair
import org.apache.http.util.EntityUtils
import com.google.common.base.Charsets
import com.google.common.base.Splitter
import com.google.common.collect.Lists
import com.google.common.util.concurrent.AsyncFunction
import com.google.common.util.concurrent.Futures
import com.google.common.util.concurrent.ListenableFuture
import com.google.common.util.concurrent.SettableFuture
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonArray
import com.google.gson.JsonParseException
import com.google.gson.JsonParser
//remove if not needed
import scala.collection.JavaConversions._
/**
 * Created by shivi on 11/29/14.
 */
object EtcdClient {

  val httpClient = buildDefaultHttpClient()

  val gson = new GsonBuilder().create()

  def main(args: Array[String]) {
    this.client = new EtcdClient(URI.create("http://127.0.0.1:4001/"))
    val key = "shivi_message"
    var result: EtcdResult = null
    result = this.client.set(key, "hello")
    result = this.client.get(key)
    println(result.node.value)
  }

  def buildDefaultHttpClient(): CloseableHttpAsyncClient = {
    val requestConfig = RequestConfig.custom().build()
    val httpClient = HttpAsyncClients.custom().setDefaultRequestConfig(requestConfig)
      .build()
    httpClient.start()
    httpClient
  }
 // val baseUri: URI = _

  def this(baseUri: URI)
    {
      this()
      var uri = baseUri.toString

      if (!uri.endsWith("/")) {
        uri += "/"
        baseUri = URI.create(uri)
      }
    }

  def get(key: String): EtcdResult = {
    val uri = buildKeyUri("v2/keys", key, "")
    val request = new HttpGet(uri)
    val result = syncExecute(request, Array(200, 404), 100)
    if (result.isError) {
      if (result.errorCode == 100) {
        return null
      }
    }
    result
  }

  def delete(key: String): EtcdResult = {
    val uri = buildKeyUri("v2/keys", key, "")
    val request = new HttpDelete(uri)
    syncExecute(request, Array(200, 404))
  }

  def set(key: String, value: String): EtcdResult = set(key, value, null)

  def set(key: String, value: String, ttl: java.lang.Integer): EtcdResult = {
    val data = Lists.newArrayList()
    data.add(new BasicNameValuePair("value", value))
    if (ttl != null) {
      data.add(new BasicNameValuePair("ttl", java.lang.Integer.toString(ttl)))
    }
    set0(key, data, Array(200, 201))
  }

  def createDirectory(key: String): EtcdResult = {
    val data = Lists.newArrayList()
    data.add(new BasicNameValuePair("dir", "true"))
    set0(key, data, Array(200, 201))
  }

  def listDirectory(key: String): List[EtcdNode] = {
    val result = get(key + "/")
    if (result == null || result.node == null) {
      return null
    }
    result.node.nodes
  }

  def deleteDirectory(key: String): EtcdResult = {
    val uri = buildKeyUri("v2/keys", key, "?dir=true")
    val request = new HttpDelete(uri)
    syncExecute(request, Array(202))
  }

  def cas(key: String, prevValue: String, value: String): EtcdResult = {
    val data = Lists.newArrayList()
    data.add(new BasicNameValuePair("value", value))
    data.add(new BasicNameValuePair("prevValue", prevValue))
    set0(key, data, Array(200, 412), 101)
  }

  def watch(key: String): ListenableFuture[EtcdResult] = watch(key, null, false)

  def watch(key: String, index: java.lang.Long, recursive: Boolean): ListenableFuture[EtcdResult] = {
    var suffix = "?wait=true"
    if (index != null) {
      suffix += "&waitIndex=" + index
    }
    if (recursive) {
      suffix += "&recursive=true"
    }
    val uri = buildKeyUri("v2/keys", key, suffix)
    val request = new HttpGet(uri)
    asyncExecute(request, Array(200))
  }

  def getVersion(): String = {
    val uri = baseUri.resolve("version")
    val request = new HttpGet(uri)
    val s = syncExecuteJson(request, 200)
    if (s.httpStatusCode != 200) {
      throw new EtcdClientException("Error while fetching versions", s.httpStatusCode)
    }
    s.json
  }

  private def set0(key: String,
                   data: List[BasicNameValuePair],
                   httpErrorCodes: Array[Int],
                   expectedErrorCodes: Int*): EtcdResult = {
    val uri = buildKeyUri("v2/keys", key, "")
    val request = new HttpPut(uri)
    val entity = new UrlEncodedFormEntity(data, Charsets.UTF_8)
    request.setEntity(entity)
    syncExecute(request, httpErrorCodes, expectedErrorCodes)
  }

  def listChildren(key: String): EtcdResult = {
    val uri = buildKeyUri("v2/keys", key, "/")
    val request = new HttpGet(uri)
    val result = syncExecute(request, Array(200))
    result
  }

  protected def asyncExecute(request: HttpUriRequest, expectedHttpStatusCodes: Array[Int], expectedErrorCodes: Int*): ListenableFuture[EtcdResult] = {
    val json = asyncExecuteJson(request, expectedHttpStatusCodes)
    Futures.transform(json, new AsyncFunction[JsonResponse, EtcdResult]() {

      def apply(json: JsonResponse): ListenableFuture[EtcdResult] = {
        val result = jsonToEtcdResult(json, expectedErrorCodes)
        Futures.immediateFuture(result)
      }
    })
  }

  protected def syncExecute(request: HttpUriRequest, expectedHttpStatusCodes: Array[Int], expectedErrorCodes: Int*): EtcdResult = {
    try {
      asyncExecute(request, expectedHttpStatusCodes, expectedErrorCodes)
        .get
    } catch {
      case e: InterruptedException => {
        Thread.currentThread().interrupt()
        throw new EtcdClientException("Interrupted during request", e)
      }
      case e: ExecutionException => throw unwrap(e)
    }
  }

  private def unwrap(e: ExecutionException): EtcdClientException = {
    val cause = e.getCause
    if (cause.isInstanceOf[EtcdClientException]) {
      return cause.asInstanceOf[EtcdClientException]
    }
    new EtcdClientException("Error executing request", e)
  }

  private def jsonToEtcdResult(response: JsonResponse, expectedErrorCodes: Int*): EtcdResult = {
    if (response == null || response.json == null) {
      return null
    }
    val result = parseEtcdResult(response.json)
    if (result.isError) {
      if (!contains(expectedErrorCodes, result.errorCode)) {
        throw new EtcdClientException(result.message, result)
      }
    }
    result
  }

  private def parseEtcdResult(json: String): EtcdResult = {
    var result: EtcdResult = null
    result = gson.fromJson(json, classOf[EtcdResult])
    result
  }

  private def contains(list: Array[Int], find: Int): Boolean = {
    (0 until list.length).find(list(_) == find).map(_ => true)
      .getOrElse(false)
  }

  protected def syncExecuteList(request: HttpUriRequest): List[EtcdResult] = {
    val response = syncExecuteJson(request, 200)
    if (response.json == null) {
      return null
    }
    if (response.httpStatusCode != 200) {
      val etcdResult = parseEtcdResult(response.json)
      throw new EtcdClientException("Error listing keys", etcdResult)
    }
    val ret = new ArrayList[EtcdResult]()
    val parser = new JsonParser()
    val array = parser.parse(response.json).getAsJsonArray
    for (i <- 0 until array.size) {
      val next = gson.fromJson(array.get(i), classOf[EtcdResult])
      ret.add(next)
    }
    ret
  }

  protected def syncExecuteJson(request: HttpUriRequest, expectedHttpStatusCodes: Int*): JsonResponse = {
    try {
      asyncExecuteJson(request, expectedHttpStatusCodes).get
    } catch {
      case e: InterruptedException => {
        Thread.currentThread().interrupt()
        throw new EtcdClientException("Interrupted during request processing", e)
      }
      case e: ExecutionException => throw unwrap(e)
    }
  }

  protected def asyncExecuteJson(request: HttpUriRequest, expectedHttpStatusCodes: Array[Int]): ListenableFuture[JsonResponse] = {
    val response = asyncExecuteHttp(request)
    Futures.transform(response, new AsyncFunction[HttpResponse, JsonResponse]() {

      def apply(httpResponse: HttpResponse): ListenableFuture[JsonResponse] = {
        val json = extractJsonResponse(httpResponse, expectedHttpStatusCodes)
        Futures.immediateFuture(json)
      }
    })
  }

  class JsonResponse(val json: String, statusCode: Int) {

    val httpStatusCode = statusCode
  }

  protected def extractJsonResponse(httpResponse: HttpResponse, expectedHttpStatusCodes: Array[Int]): JsonResponse = {
    try {
      val statusLine = httpResponse.getStatusLine
      val statusCode = statusLine.getStatusCode
      var json: String = null
      if (httpResponse.getEntity != null) {
        json = EntityUtils.toString(httpResponse.getEntity)
      }
      if (!contains(expectedHttpStatusCodes, statusCode)) {
        if (statusCode == 400 && json != null) {
        } else {
          throw new EtcdClientException("Error response from etcd: " + statusLine.getReasonPhrase, statusCode)
        }
      }
      new JsonResponse(json, statusCode)
    } finally {
      close(httpResponse)
    }
  }

  private def buildKeyUri(prefix: String, key: String, suffix: String): URI = {
    val sb = new StringBuilder()
    sb.append(prefix)
    if (key.startsWith("/")) {
      key = key.substring(1)
    }
    for (token <- Splitter.on('/').split(key)) {
      sb.append("/")
      sb.append(urlEscape(token))
    }
    sb.append(suffix)
    val uri = baseUri.resolve(sb.toString)
    uri
  }


  protected def asyncExecuteHttp(request: HttpUriRequest): ListenableFuture[HttpResponse] = {
    val future = SettableFuture.create()
    httpClient.execute(request, new FutureCallback[HttpResponse]() {

      def completed(result: HttpResponse) {
        future.set(result)
      }

      def failed(ex: Exception) {
        future.setException(ex)
      }

      def cancelled() {
        future.setException(new InterruptedException())
      }
    })
    future
  }

  def close(response: HttpResponse) {
    if (response == null) {
      return
    }
    val entity = response.getEntity
    if (entity != null) {
      EntityUtils.consumeQuietly(entity)
    }
  }

  protected def urlEscape(s: String): String = URLEncoder.encode(s, "UTF-8")

  def format(o: AnyRef): String = {
    try {
      gson.toJson(o)
    } catch {
      case e: Exception => "Error formatting: " + e.getMessage
    }
  }

}
