package etcd

import EtcdJsonProtocol.EtcdResponse

import scala.concurrent.Future
import scala.util.{Failure, Success}
import scala.concurrent.ExecutionContext.Implicits.global


/**
 * Created by shivi on 11/29/14.
 */
class Etcd {

  val client = new EtcdClient("http://54.183.71.110:4001")
  val counterKey = "010038327/counter"

  def IncrementCounter(): Future[EtcdResponse] =
  {
    var response: Future[EtcdResponse] = client.getKey(counterKey)

    response onComplete {

      case Success(response: EtcdResponse) => {
        val counterVal = response.node.value

        counterVal match {

          case Some(value) => {
            updateCounter((value.toInt + 1).toString, value, "true")
          }

          case None => {
            println("Failed: " + counterVal)
          }
        }
      }

      case Failure(error) => {
        updateCounter("1", "0", "false")
      }
    }

    return response
  }

  def getCounter(): Future[EtcdResponse] =
  {
    return client.getKey(counterKey)
  }


  def updateCounter(value: String, prevValue: String, prevExist: String): Unit = {

    var setresponse: Future[EtcdResponse] = null

    if (prevExist == "true") {
      setresponse = client.setKeyPrevValue(counterKey, value, prevValue)
    } else {
      setresponse = client.setKeyPrevExist(counterKey, value, prevExist)
    }

    setresponse onComplete {

      case Success(setresponse: EtcdResponse) => {}

      case Failure(error) => {
       IncrementCounter()
      }

    }
  }

  def randomTime(): Int = {
    var r = new scala.util.Random
    return r.nextInt(10)
  }
}
