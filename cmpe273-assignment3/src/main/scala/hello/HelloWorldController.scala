package hello

import javax.validation.Valid
import java.security.MessageDigest

import etcd.Etcd
import org.hibernate.validator.constraints.{NotEmpty, Email}
import org.springframework.beans.factory.annotation.Required
import org.springframework.boot._
import org.springframework.boot.autoconfigure._
import org.springframework.http.{HttpStatus, ResponseEntity, HttpHeaders}
import org.springframework.stereotype._
import org.springframework.util.DigestUtils
import org.springframework.validation.{FieldError, BindingResult}
import org.springframework.web.bind.annotation._
import java.util._

import scala.beans.BeanProperty
import scala.collection.JavaConversions._

import com.google.gson.GsonBuilder;
import com.google.gson.Gson

import etcd.EtcdJsonProtocol.EtcdResponse

import scala.concurrent._
import scala.util.{Failure, Success}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._


class UserRegistrationRequest {


  @BeanProperty
  @NotEmpty(message = "email cannot be null")
  @Email(message = "email not valid")
  var email: String = _

  @BeanProperty
  @NotEmpty(message = "Please enter your password.")
  var password: String = _

  @BeanProperty
  var name: String = _
}

class IdCardRegistrationRequest {

  @BeanProperty
  @NotEmpty(message = "card name cannot be null")
  var card_name: String = _

  @BeanProperty
  @NotEmpty(message = "card number cannot be null")
  var card_number: String = _

  @BeanProperty
  var expiration_date: String = _
}

class UserWebLoginRequest {

  @BeanProperty
  @NotEmpty(message = "url cannot be null")
  var url: String = _

  @BeanProperty
  @NotEmpty(message = "login cannot be null")
  var login: String = _

  @BeanProperty
  @NotEmpty(message = "password cannot be null")
  var password: String = _
}
class BankAccountRequest {

  @BeanProperty
  var account_name: String = _

  @BeanProperty
  @NotEmpty(message = "routing number cannot be null")
  var routing_number: String = _

  @BeanProperty
  @NotEmpty(message = "account number cannot be null")
  var account_number: String = _
}


@RestController
@EnableAutoConfiguration
@RequestMapping(value=Array("/api/v1"))
class HelloWorldController {

  var userdata = new AllUsers
  var cardsdata = new AllIDCards
  var userweblogin = new AllWebLogins
  var bankdata = new AllBankAccounts
  var errormsg = ""
  var etcdmain = new Etcd

  @RequestMapping(value=Array("/health"), method=Array(RequestMethod.GET))
  def health() = {
    new ResponseEntity[String](null, new HttpHeaders(), HttpStatus.OK)
  }

  @RequestMapping(value=Array("/counter"), method=Array(RequestMethod.GET))
  def counter(): ResponseEntity[String] = {

    val updateFuture = etcdmain.IncrementCounter()
    val response = Await.result(updateFuture, 25 seconds)

    val counterVal = response.node.value

    counterVal match {

      case Some(value) => {
        return new ResponseEntity[String]((value.toInt + 1).toString, new HttpHeaders(), HttpStatus.OK)
      }

      case None => {
        return new ResponseEntity[String](null, new HttpHeaders(), HttpStatus.NOT_FOUND)
      }
    }
  }

  @RequestMapping(value=Array("/users/{user_id}"), method=Array(RequestMethod.GET))
  def users(
         @PathVariable user_id : String,
         @RequestHeader(value="ETag", defaultValue="") etag: String,
         @RequestHeader(value="If-None-Match", defaultValue="") ifNoneMatch: String) = {

      etcdmain.IncrementCounter()

      val responseHeaders = new HttpHeaders()
      val gson = new GsonBuilder().setPrettyPrinting().create();
      val getjson = gson.toJson(userdata.map(user_id))

      val newETag = new String(md5(getjson))
      responseHeaders.set("ETag", newETag)

      val newresponse = new ResponseEntity[String](getjson, responseHeaders, HttpStatus.OK)
      val existingresponse = new ResponseEntity[String](getjson, responseHeaders, HttpStatus.NOT_MODIFIED)

      if(etag == "" && ifNoneMatch == "")
        {
          newresponse
        }
      else if(etag ==  newETag || ifNoneMatch == newETag)
        {
          existingresponse
        }
      else
       newresponse
  }

  def getBadRequestResponse(result: BindingResult) =
  {

      errormsg = ""
      for ( error : FieldError  <- result.getFieldErrors() )
        {
           errormsg = errormsg.concat(error.getDefaultMessage() + "\n")
        }
      new ResponseEntity[String](errormsg, new HttpHeaders(),HttpStatus.BAD_REQUEST)
  }

  @RequestMapping(value=Array("/users"), method=Array(RequestMethod.POST))
  def users(@Valid @RequestBody request : UserRegistrationRequest, result: BindingResult) =
  {
    val gson = new GsonBuilder().setPrettyPrinting().create();
    if(result.hasErrors)
      {
        getBadRequestResponse(result)
      } else {


      val usernow = new User(randomID("u"),request.email, request.password, request.name, currentDate(), "");
      userdata.SaveData(usernow);
      val getjson = gson.toJson(usernow)
      val existingresponse = new ResponseEntity[String](getjson, new HttpHeaders(), HttpStatus.CREATED)
      existingresponse
      }

  }

  @RequestMapping(value=Array("/users/{user_id}"), method=Array(RequestMethod.PUT))
  def users(@PathVariable user_id : String, @Valid @RequestBody request : UserRegistrationRequest,result: BindingResult) =
  {
    val gson = new GsonBuilder().setPrettyPrinting().create();
    if(result.hasErrors)
      {
        getBadRequestResponse(result)
      }
    else{

    val user_old = userdata.map(user_id)
    val user_updated = new User(user_id, request.email, request.password, request.name, user_old.created_at, currentDate());
    userdata.SaveData(user_updated);
      var getjson = gson.toJson(user_updated)
      new ResponseEntity[String](getjson, new HttpHeaders(), HttpStatus.CREATED)
     //user_updated;
      //getjson
      }
  }

  @RequestMapping(value=Array(" /users/{user_id}/idcards"), method=Array(RequestMethod.POST))
  def idcards(@PathVariable user_id : String,@Valid @RequestBody request : IdCardRegistrationRequest,result: BindingResult) =
  {
    val gson = new GsonBuilder().setPrettyPrinting().create();
    if(result.hasErrors)
      {
        getBadRequestResponse(result)
      }
    else{

    val card = new IDCard(randomID("c"),request.card_name, request.card_number, request.expiration_date);
    cardsdata.SaveData(user_id, card);
      var getjson = gson.toJson(card)
      new ResponseEntity[String](getjson, new HttpHeaders(), HttpStatus.CREATED)
     //getjson;
      }
  }
  @RequestMapping(value=Array(" /users/{user_id}/idcards"), method=Array(RequestMethod.GET))
  def idcards(@PathVariable user_id : String) =
  {
    etcdmain.IncrementCounter()
    val gson = new GsonBuilder().setPrettyPrinting().create();
    var getjson = gson.toJson(cardsdata.map(user_id).getCards())
    new ResponseEntity[String](getjson, new HttpHeaders(), HttpStatus.OK)
    getjson
  }
  @RequestMapping(value=Array("/users/{user_id}/idcards/{card_id}"), method=Array(RequestMethod.DELETE))
  def idcards(@PathVariable user_id : String, @PathVariable card_id : String) =
  {
    cardsdata.DeleteCard(user_id, card_id);
    new ResponseEntity[String](null, new HttpHeaders(),HttpStatus.NO_CONTENT)
  }

  @RequestMapping(value=Array("/users/{user_id}/weblogins"), method=Array(RequestMethod.POST))
  def weblogin(@PathVariable user_id : String,@Valid @RequestBody request : UserWebLoginRequest,result: BindingResult) =
  {
     val gson = new GsonBuilder().setPrettyPrinting().create();
     if(result.hasErrors)
       {
         getBadRequestResponse(result)
       }
    else{
    val weblogin = new WebLogin(randomID("l"),request.url,request.login,request.password);
    userweblogin.SaveData(user_id, weblogin);
       var getjson = gson.toJson(weblogin)
       new ResponseEntity[String](getjson, new HttpHeaders(), HttpStatus.CREATED)
       }
  }

  @RequestMapping(value=Array(" /users/{user_id}/weblogins"), method=Array(RequestMethod.GET))
  def weblogins(@PathVariable user_id : String) =
  {
    etcdmain.IncrementCounter()
    val gson = new GsonBuilder().setPrettyPrinting().create();

    var getjson = gson.toJson(userweblogin.map(user_id).getWebLogins())
    new ResponseEntity[String](getjson, new HttpHeaders(), HttpStatus.OK)
  }

  @RequestMapping(value=Array("/users/{user_id}/weblogins/{login_id}"), method=Array(RequestMethod.DELETE))
  def weblogins(@PathVariable user_id : String, @PathVariable login_id : String) =
  {
    userweblogin.DeleteWebLogin(user_id, login_id);
    new ResponseEntity[String](null, new HttpHeaders(), HttpStatus.NO_CONTENT)
  }

  @RequestMapping(value=Array("/users/{user_id}/bankaccounts"), method=Array(RequestMethod.POST))
  def bankaccounts(@PathVariable user_id : String,@Valid @RequestBody request : BankAccountRequest,result: BindingResult) =
  {
    val gson = new GsonBuilder().setPrettyPrinting().create();
     if(result.hasErrors)
       {
         getBadRequestResponse(result)
       }
     else{
     val bankacc = new BankAccount(randomID("b"),request.account_name, request.routing_number, request.account_number);
     bankdata.SaveData(user_id, bankacc);
       var getjson = gson.toJson(bankacc)
       new ResponseEntity[String](getjson, new HttpHeaders(), HttpStatus.CREATED)
      //getjson;
       }
  }

  @RequestMapping(value=Array("/users/{user_id}/bankaccounts"), method=Array(RequestMethod.GET))
  def bankaccounts(@PathVariable user_id : String) =
  {
    etcdmain.IncrementCounter()
    val gson = new GsonBuilder().setPrettyPrinting().create();
    var getjson = gson.toJson(bankdata.map(user_id).getBankAccounts())
    new ResponseEntity[String](getjson, new HttpHeaders(), HttpStatus.OK)
  }

  @RequestMapping(value=Array("/users/{user_id}/bankaccounts/{ba_id}"), method=Array(RequestMethod.DELETE))
  //@RequestBody
  def bankaccounts(@PathVariable user_id : String, @PathVariable ba_id : String) =
  {
    bankdata.DeleteBankAccount(user_id, ba_id);
    new ResponseEntity[String](null, new HttpHeaders(), HttpStatus.NO_CONTENT)
  }


  def randomID(prefix: String): String = {
    var r = new scala.util.Random
    return prefix + "-" + (r.nextInt(Integer.MAX_VALUE)).toString
  }

  def currentDate(): String = {
    val format = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    format.format(new java.util.Date())
  }

  def md5(s: String) = {
    DigestUtils.md5DigestAsHex(s.getBytes());
  }
}
