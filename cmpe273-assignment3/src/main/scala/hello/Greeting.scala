package hello

class Greeting{
    var id = 0;
    var content = "";
    def this(id1 : Int , content1 : String)
    {
      this();
        this.id = id1;
        this.content = content1;
    }
    def getId() : Int =
    {
       return id;
    }

    def getContent() : String =
    {
        return content;
    }
}
