package hello

class IDCard{
    var card_id = "";//system generated
    var card_name = "";
    var card_number = "";
    var expiration_date = "";//datetime
    def this(card_id : String , card_name : String , card_number : String , expiration_date : String)
    {
      this();
      this.card_id = card_id;
      this.card_name = card_name;
      this.card_number = card_number;
      this.expiration_date = expiration_date;
    }
  def getCard_id() : String =
  {
    return card_id;
  }
  def getCard_name() : String =
  {
    return  card_name;
  }
  def getCard_number() : String = {
    return card_number;
  }
  def getExpiration_date() : String = {
    return  expiration_date;
  }


}
